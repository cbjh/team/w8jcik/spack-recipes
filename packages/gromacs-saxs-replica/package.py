# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.pkg.builtin.gromacs import Gromacs


class GromacsSaxsReplica(Gromacs):
    """Modified Gromacs for small-angle scattering calculations (SAXS/WAXS/SANS)"""

    homepage = 'https://biophys.uni-saarland.de/swaxs.html'
    url = 'https://gitlab.com/cbjh/gromacs-swaxs/-/archive/release-2019.swaxs-0.1/gromacs-swaxs-release-2019.swaxs-0.1.tar.bz2'
    git = 'https://gitlab.com/cbjh/gromacs-swaxs.git'
    maintainers = ['w8jcik']

    version('2018.8', branch='master-maxent-2018b')

    conflicts('+plumed')
    conflicts('+opencl')
    conflicts('+sycl')

    def remove_parent_versions(self):
        """
        By inheriting GROMACS package we also inherit versions.
        They are not valid, so we are removing them.
        """

        for version_key in Gromacs.versions.keys():
            if version_key in self.versions:
                if version_key != Version('2018.8'):
                    del self.versions[version_key]

    def __init__(self, spec):
        super(GromacsSaxsReplica, self).__init__(spec)

        self.remove_parent_versions()
